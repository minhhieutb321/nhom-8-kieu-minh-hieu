﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        int max = int.MinValue + 1;
        int maxSecond = int.MinValue;
        if (x.Length < 2) return -1;


        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > max)
            {
                maxSecond = max;
                max = x[i];
            }
            else if (x[i] == max)
            {
                return i;
            }
            else if (x[i] > maxSecond && x[i] < max)
            {
                maxSecond = x[i];
            }

        }

        if (max == x[0] && maxSecond == x[1]) return 1;
        return Array.IndexOf(x, maxSecond);
    }
}