﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        if (string.IsNullOrEmpty(value1) && string.IsNullOrEmpty(value2)) return string.Empty;
        if (!string.IsNullOrEmpty(value1) && string.IsNullOrEmpty(value2)) return value1;
        if (string.IsNullOrEmpty(value1) && !string.IsNullOrEmpty(value2)) return value2;
        string result = null;
        char[] arrayChar1 = value1.ToCharArray();
        char[] arrayChar2 = value2.ToCharArray();

        int index = 0;
        int lenght = 0;
        bool check = arrayChar1.Length <= arrayChar2.Length ? true : false;
        if (check)
        {
            lenght = arrayChar2.Length;
        }
        else
        {
            lenght = arrayChar1.Length;
        }

        while (index < lenght)
        {
            if (check)
            {
                if (index < arrayChar1.Length)
                {
                    result += arrayChar1[index].ToString() + arrayChar2[index].ToString();
                }
            }
            else
            {
                if (index < arrayChar2.Length)
                {
                    result += arrayChar1[index].ToString() + arrayChar2[index].ToString();
                }
            }

            if (check)
            {
                if (index >= arrayChar1.Length)
                {
                    result += arrayChar2[index].ToString();
                }
            }
            else
            {
                if (index >= arrayChar2.Length)
                {
                    result += arrayChar1[index].ToString();
                }
            }
            index++;
        }

        return result;
    }
}

